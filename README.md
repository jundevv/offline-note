# Jun Notes

Simple Offline Note with React, Apollo, GraphQL


## List
![List](./img/List_of_Jun-Note_screenshot.jpg)

## Edit
![Edit](./img/Edit_of_Jun-Note_screenshot.jpg)

## Detail
![Detail](./img/Detail_of_Jun-Note_screenshot.jpg)
