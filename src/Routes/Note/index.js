import React from "react";
import { Query } from "react-apollo";
import Note from "../../Components/Note";
import { GET_NOTE } from "../../queries";


export default (props) => (
  <Query 
    query={GET_NOTE} 
    variables={{ id: props.match.params.id }}
  >
    {({ data }) => 
      data.note ? (
        <Note 
          id={data.note.id}
          title={data.note.title}
          content={data.note.content}
        />
      ) : null
    } 
  </Query>
)