import React from "react";
import { Query, Mutation } from "react-apollo";
import Editor from "../../Components/Editor";
import { GET_NOTE, EDIT_NOTE } from "../../queries";

export default (props) => 
  <Query 
    query={GET_NOTE}
    variables={{ id: props.match.params.id }}
  >
    {({ data }) => 
      data.note ? (
        <Mutation mutation={EDIT_NOTE}>{
          (editNote) => <Editor
                          id={data.note.id}
                          title={data.note.title}
                          content={data.note.content}
                          onSave={(title, content, id)=>{
                            if (title !== '' && content !== '' && id){
                              editNote({
                                variables: {title, content, id}
                              })
                              props.history.push(`/note/${data.note.id}`)
                            }
                          }}
                        />
        }
        </Mutation>
      ) : null
    } 
  </Query>