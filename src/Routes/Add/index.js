import React from "react";
import { Mutation } from "react-apollo";
import Editor from "../../Components/Editor";
import { ADD_NOTE } from "../../queries";

export default (props) =>
<Mutation mutation={ADD_NOTE}>
  {addNote => 
      <Editor 
        onSave={(title, content)=>{
          if (title !== '' && content !== ''){
            addNote({ variables: { title, content }})
            props.history.push("/")
          }
        }} 
      />
  }
</Mutation>