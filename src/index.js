import React from 'react';
import App from './Components/App';
import ReactDOM from 'react-dom';
import { ApolloProvider } from "react-apollo";
import client from "./apollo";
import GlobalStyle from "./globalStyles";

ReactDOM.render(
  <ApolloProvider client={client}>
    <App />
    <GlobalStyle />
  </ApolloProvider>, 
document.getElementById('root'));
