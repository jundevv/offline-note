import { GET_NOTES } from "./queries";
const KEY = "JunNotes"
export const saveNotes = cache => {
  const { notes } = cache.readQuery({ query: GET_NOTES});
  const jsonNotes = JSON.stringify(notes)
  try{
    localStorage.setItem(KEY, jsonNotes);
  } catch(error) {
	  console.log(error)
  }
}

export const restoreNotes = () => {
  let notes = []
  const notesJson = localStorage.getItem(KEY)
  if(notesJson) {
    try{
      notes = JSON.parse(notesJson)
    } catch(error) {
      console.log(error)
    }
  }
  return notes
}