import React from 'react';
import { BrowserRouter, Switch, Route } from "react-router-dom";
import Notes from '../../Routes/Notes';
import Edit from '../../Routes/Edit';
import Add from '../../Routes/Add';
import Note from '../../Routes/Note';


function App() {
  return (
      <BrowserRouter>
        <Switch>
          <Route exact={true} path={"/"} component={Notes} />
          <Route exact={true} path={"/add"} component={Add} />
          <Route exact={true} path={"/edit/:id"} component={Edit} />
          <Route exact={true} path={"/note/:id"} component={Note} />
        </Switch>
      </BrowserRouter>
  );
}

export default App;
