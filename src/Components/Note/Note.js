
import React from "react";
import styled from "styled-components";
import MarkdownRenderer from "react-markdown-renderer";
import { Link } from "react-router-dom";


const TitleComponent = styled.div`
  display: -webkit-box;
  align-items: center;
  justify-content: space-between;
  margin-bottom: 50px;
`;

const Title = styled.h1`
  font-size: 50px;
  margin: 0;
  padding: 0;
`;

const Button = styled.button``;

export default (props) => 
  <>
    <TitleComponent>
      <Title>{props.title}</Title>
      <Link to={'/'} style={{marginRight: '5px'}}>
        <Button>List</Button>
      </Link>
      <Link to={`/edit/${props.id}`}>
        <Button>
          Edit
        </Button>
      </Link>
    </TitleComponent>
    <MarkdownRenderer markdown={props.content} />
  </>